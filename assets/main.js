/* Preenchimento padrão do campos de settings seguindo a proposta
bloquear360000526812:custom_field_360022552932
esconderDenuncia:custom_field_360023685372,custom_field_360022580531
bloquear360000568152:
esconder360000568152:
bloquear360000533791:
esconder360000533791:
bloquear360000533811:
esconder360000533811:
bloquear360000526452:custom_field_360022551172
esconder360000526452:
bloquear360000533271:
esconder360000533271:
*/
var userid;
var autor;
var client = ZAFClient.init();
var alterado=false;
var bloquear360000526812 
var bloquear360000568152 
var bloquear360000533791 
var bloquear360000533811 
var bloquear360000526452 
var bloquear360000533271 
var esconder360000526812 
var esconder360000568152
var esconder360000533791
var esconder360000533811
var esconder360000526452
var esconder360000533271
var bloquear360000665911
var esconder360000665911
var bloquearsempre
var escondersempre
var bloquearcriacao
var escondercriacao
var subject
var codigoorigem = 360022799832
var formid;
$( function () {
    client.get("ticket.form.id").then(function (dataid) {
        formid=dataid["ticket.form.id"]
        principal()
    })
    
client.on('ticket.subject.changed', function(user) {

        if(userid!=autor)
        client.set('ticket.subject', subject)
  });
  client.on('ticket.form.id.changed',function(user) {
    client.get("ticket.form.id").then(function (dataid) {

        formid=dataid["ticket.form.id"]
        principal()
        // console.log(formid)
    })

   
});
  
})

function locksempre(datasettings,datafields){
                        if(datasettings.settings.bloquearsempre!=undefined)
                        bloquearsempre=datasettings.settings.bloquearsempre.split(',');
                        if(datasettings.settings.escondersempre!=undefined)
                        escondersempre=datasettings.settings.escondersempre.split(',');
                        locks(datafields,bloquearsempre,escondersempre)
}

function locks(datafields,bloquear,esconder){
    var tamanho = datafields.ticketFields.length;
    for(i=0;i<tamanho;i++){
    if(bloquear!=undefined){    
    for(j=0;j<bloquear.length;j++){
        if(datafields.ticketFields[i].name==bloquear[j]){
            client.invoke("ticketFields."+i+".disable")
            j=bloquear.length
          
        }
      }}
      if(esconder!=undefined){
      for(j=0;j<esconder.length;j++){
        if(datafields.ticketFields[i].name==esconder[j]){
            client.invoke("ticketFields."+i+".hide")
            j=esconder.length
            
        } 
      }}

    }
}
function lock(datafields,bloquear,esconder){
    var tamanho = datafields.ticketFields.length;
    var ativo=true
    for(i=0;i<tamanho;i++){
        ativo=true
    if(bloquear!=undefined){    
    for(j=0;j<bloquear.length;j++){
        if(datafields.ticketFields[i].name==bloquear[j]){
            client.invoke("ticketFields."+i+".disable")
            j=bloquear.length
            ativo=false
            console.log('bloquear: '+datafields.ticketFields[i].name)
        }
      }}
      if(esconder!=undefined){
      for(j=0;j<esconder.length;j++){
        if(datafields.ticketFields[i].name==esconder[j]){
            client.invoke("ticketFields."+i+".hide")
            j=esconder.length
            ativo=false
            console.log('esconder: '+datafields.ticketFields[i].name)
        } 
      }}
      if(ativo==true){
      client.invoke("ticketFields."+i+".enable")
      console.log('ativar: '+datafields.ticketFields[i].name)
    }
    }
}

function isfieldorigem(field){
    return field.id==codigoorigem
  }

function principal(){

    client.metadata().then(function(datasettings) {
    // console.log(datasettings)
        
    client.invoke('resize', { width: '100%', height: '0px' });
    client.get('currentUser').then(function(datauser){
      user=datauser.currentUser;
          client.get('ticket.id').then(function(dataid){
          id = dataid["ticket.id"]
          var settings = {
          url: "/api/v2/tickets/"+id+".json",
          type: 'GET',
          dataType: 'json',
          contentType: 'application/json',
          };
          client.get('ticketFields').then(function(datafields){
            // console.log(datafields);
                  client.request(settings).then(function(dataticket){
                
                    var valororigem = dataticket.ticket.custom_fields.find(isfieldorigem).value
                    subject=dataticket.ticket.subject;
                    //console.log(valororigem)
                    // console.log(dataticket)
                    autor = dataticket["ticket"]["submitter_id"];
                    userid=user['id']
                    if(user["id"]==autor ){
                        // console.log("teste")
                        if(datasettings.settings.bloquearcriacao!=undefined)
                        bloquearcriacao=datasettings.settings.bloquearcriacao.split(',');
                        if(datasettings.settings.escondercriacao!=undefined)
                        escondercriacao=datasettings.settings.escondercriacao.split(',');
                        client.get('ticketFields').then(function(datafields){
                        lock(datafields,bloquearcriacao,escondercriacao)})
                    //$("#autor").show(); 
                    }else if(valororigem=='sistema'){
                    //$("#nautor").show();
                    
                    if(formid=='360000526812'){       // Fomulario de Denuncia
                        if(datasettings.settings.bloquear360000526812!=undefined)
                            bloquear360000526812=datasettings.settings.bloquear360000526812.split(',');
                        if(datasettings.settings.esconder360000526812!=undefined)
                            esconder360000526812=datasettings.settings.esconder360000526812.split(',');
                        lock(datafields,bloquear360000526812,esconder360000526812)
                        //console.log("Denuncia")
                    }else if(formid=='360000568152'){       // Fomulario de Duvida
                        if(datasettings.settings.bloquear360000568152!=undefined)
                            bloquear360000568152=datasettings.settings.bloquear360000568152.split(',');
                        if(datasettings.settings.esconder360000568152!=undefined)
                            esconder360000568152=datasettings.settings.esconder360000568152.split(',');
                        lock(datafields,bloquear360000568152,esconder360000568152)
                        //console.log("Duvida")
                    }else if(formid=='360000533791'){       // Fomulario de Elogio
                        if(datasettings.settings.bloquear360000533791!=undefined)
                            bloquear360000533791=datasettings.settings.bloquear360000533791.split(',');
                        if(datasettings.settings.esconder360000533791!=undefined)
                            esconder360000533791=datasettings.settings.esconder360000533791.split(',');
                        lock(datafields,bloquear360000533791,esconder360000533791)
                        //console.log("Elogio")
                    }else if(formid=='360000533811'){       // Fomulario de Sugestão
                        if(datasettings.settings.bloquear360000533811!=undefined)
                            bloquear360000533811=datasettings.settings.bloquear360000533811.split(',');
                        if(datasettings.settings.esconder360000533811!=undefined)
                            esconder360000533811=datasettings.settings.esconder360000533811.split(',');
                        lock(datafields,bloquear360000533811,esconder360000533811)
                        //console.log("Sugestao")
                    }else if(formid=='360000526452'){       // Fomulario de Reclamação
                        if(datasettings.settings.bloquear360000526452!=undefined)
                            bloquear360000526452=datasettings.settings.bloquear360000526452.split(',');
                        if(datasettings.settings.esconder360000526452!=undefined)
                            esconder360000526452=datasettings.settings.esconder360000526452.split(',');
                        lock(datafields,bloquear360000526452,esconder360000526452)
                        //console.log("Reclamacao")
                    }else if(formid=='360000533271'){       // Fomulario de Solicitação
                        if(datasettings.settings.bloquear360000533271!=undefined)
                            bloquear360000533271=datasettings.settings.bloquear360000533271.split(',');
                        if(datasettings.settings.esconder360000533271!=undefined)
                            esconder360000533271=datasettings.settings.esconder360000533271.split(',');
                        lock(datafields,bloquear360000533271,esconder360000533271)
                        //console.log("Solicitacao")
                    }else if(formid=='360000665911'){
                        if(datasettings.settings.bloquear360000665911!=undefined)
                            bloquear360000665911=datasettings.settings.bloquear360000665911.split(',');
                        if(datasettings.settings.esconder360000665911!=undefined)
                            esconder360000665911=datasettings.settings.esconder360000665911.split(',');
                        lock(datafields,bloquear360000665911,esconder360000665911)
                    }
                    locksempre(datasettings,datafields)
                    }else {
                        locksempre(datasettings,datafields) 
                    }
                  });
              });
          });
      });
    });
}