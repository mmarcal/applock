var client = ZAFClient.init();
var bloquearcriacao
var escondercriacao = ['custom_field_360023685372','custom_field_360022580531']

$(function(){

main()
client.on('ticket.form.id.changed', function(user) {
    main()
})

})


function lock(datafields,bloquear,esconder){
    var tamanho = datafields.ticketFields.length;
    for(i=0;i<tamanho;i++){
    if(bloquear!=undefined){    
    for(j=0;j<bloquear.length;j++){
        if(datafields.ticketFields[i].name==bloquear[j]){
            client.invoke("ticketFields."+i+".disable")
            j=bloquear.length
          
        }
      }}
      if(esconder!=undefined){
      for(j=0;j<esconder.length;j++){
        if(datafields.ticketFields[i].name==esconder[j]){
            client.invoke("ticketFields."+i+".hide")
            j=esconder.length
            
        } 
      }}

    }
}

function main(){
    client.metadata().then(function(datasettings) {
        if(datasettings.settings.bloquearcriacao!=undefined)
        bloquearcriacao=datasettings.settings.bloquearcriacao.split(',');
        if(datasettings.settings.escondercriacao!=undefined)
        escondercriacao=datasettings.settings.escondercriacao.split(',');
        client.get('ticketFields').then(function(datafields){
            lock(datafields,bloquearcriacao,escondercriacao)

        })

    })

}